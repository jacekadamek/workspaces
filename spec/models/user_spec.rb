require 'rails_helper'

RSpec.describe User, type: :model do
  it { should  validate_presence_of :first_name }
  it { should validate_presence_of :last_name }
  it { should validate_presence_of :email }
  it { should validate_uniqueness_of :email }

  it { should have_db_index :email }

  it { should have_many :workspaces }

  it "has authentication token" do
    user = create(:user)
    expect(user.authentication_token).not_to be_blank
  end
end
