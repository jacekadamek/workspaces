FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "john#{n}.doe#{n}@mail.com" }
    password "password123"
    password_confirmation "password123"
    sequence(:first_name) { |n| "John #{n}"}
    sequence(:last_name) { |n| "Doe #{n}"}
  end
end
