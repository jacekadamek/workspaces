class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :name
      t.integer :workspace_id
      t.integer :user_id

      t.timestamps null: false
    end

    add_foreign_key :projects, :users
    add_foreign_key :projects, :workspaces
    add_index :projects, :workspace_id
  end
end
