class AddMissingIndexes < ActiveRecord::Migration
  def change
    add_foreign_key :projects, :users

    add_index :users, :email, unique: true
  end
end
