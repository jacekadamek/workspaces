class LongRunningJob
  include Sidekiq::Worker

  def perform(workspace_id)
    sleep(15 * 60)
  end
end