FactoryGirl.define do
  factory :task do
    title "Task title"
    description "MyText"
    project_id 1
    user_id 1
  end

end
