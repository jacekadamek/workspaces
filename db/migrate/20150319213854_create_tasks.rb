class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :title
      t.text :description
      t.integer :project_id
      t.integer :user_id

      t.timestamps null: false
    end

    add_foreign_key :tasks, :users
    add_index :tasks, :user_id

    add_foreign_key :tasks, :projects
    add_index :tasks, :project_id
  end
end
