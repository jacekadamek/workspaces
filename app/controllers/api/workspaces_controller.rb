module Api
  class WorkspacesController < ApiBaseController

    def index
      workspaces = Workspace.all
      render json: workspaces
    end

    def show
      workspace = Workspace.find(params[:id])
      render json: workspace
    end

    def create
      workspace = WorkspaceCreator.perform(api_user, workspace_params)
      if workspace.valid?
        render json: workspace, status: :created, location: api_workspace_url(workspace)
      else
        render json: {errors: workspace.errors}, status: :unprocessable_entity
      end
    end

    def update

    end

    def destroy

    end

    private

    def workspace_params
      params.permit(:name)
    end
  end
end