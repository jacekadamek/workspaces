Rails.application.routes.draw do
  devise_for :users
  namespace :api do
    resources :workspaces, only: [:index, :create, :update, :destroy, :show]
  end
end
