class Task < ActiveRecord::Base
  validates :title, presence: true
  validates :description, presence: true
  validates :project_id, presence: true
  validates :user_id, presence: true
  belongs_to :project
  belongs_to :user
end
