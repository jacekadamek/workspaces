require "rails_helper"

describe WorkspaceCreator do
  describe ".perform" do
    let(:user) { create(:user) }

    context "with correct workspace parameters" do
      let(:workspace_params) { attributes_for(:workspace)}
      it "returns saved workspace" do
        workspace = WorkspaceCreator.perform(user, workspace_params)
        expect(workspace).to be_persisted
      end
      it "calls long running job" do
        expect(LongRunningJob).to receive(:perform_async)
        workspace = WorkspaceCreator.perform(user, workspace_params)
      end
    end

    context "with incorrect workspace parameters" do
      let(:workspace_params) { attributes_for(:workspace, name: '')}
      it "returns not saved workspace" do
        workspace = WorkspaceCreator.perform(user, workspace_params)
        expect(workspace).not_to be_persisted
      end
      it "calls long running job" do
        expect(LongRunningJob).not_to receive(:perform_async)
        workspace = WorkspaceCreator.perform(user, workspace_params)
      end
    end
  end
end