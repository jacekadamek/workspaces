class WorkspaceCreator
  def self.perform(user, params)
    workspace = Workspace.new(params)
    workspace.user = user
    if workspace.save
      LongRunningJob.perform_async(workspace.id)
    end
    workspace
  end
end