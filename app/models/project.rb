class Project < ActiveRecord::Base
  validates :name, presence: true
  validates :workspace_id, presence: true
  validates :user_id, presence: true
  belongs_to :workspace
  belongs_to :user
end
