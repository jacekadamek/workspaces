class CreateWorkspaces < ActiveRecord::Migration
  def change
    create_table :workspaces do |t|
      t.string :name
      t.integer :user_id

      t.timestamps null: false
    end

    add_foreign_key :workspaces, :users
    add_index :workspaces, :user_id
  end
end
