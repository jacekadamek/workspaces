require "rails_helper"

describe Api::WorkspacesController do
  context "request with correct authentication token" do
    let(:user) { create(:user) }
    before { request.env['HTTP_AUTHORIZATION'] = "Token token=\"#{user.authentication_token}\"" }

    describe "GET index" do
      before do
        create(:workspace, name: "First")
        create(:workspace, name: "Second")
        create(:workspace, name: "Third")
        get :index
      end
      it "returns http status success" do
        expect(response).to have_http_status(:success)
      end
      it "returns json array of workspaces" do
        workspaces = JSON.parse(response.body)
        expect(workspaces.size).to eq(3)

        expect(workspaces[0]["name"]).to eq("First")
        expect(workspaces[1]["name"]).to eq("Second")
        expect(workspaces[2]["name"]).to eq("Third")
      end
    end

    describe "GET show" do
      before do
        workspace = create(:workspace, name: "First")
        get :show, id: workspace.id
      end
      it "returns http status success" do
        expect(response).to have_http_status(:success)
      end
      it "returns workspace json object" do
        workspace = JSON.parse(response.body)
        expect(workspace["name"]).to eq("First")
      end
    end

    describe "POST create" do
      context "with correct data" do
        it "returns success" do
          post :create, attributes_for(:workspace)
          expect(response).to have_http_status(:success)
        end
        it "returns json object of workspace" do
          post :create, attributes_for(:workspace, name: "Workspace 1")
          workspace = JSON.parse(response.body)
          expect(workspace["name"]).to eq("Workspace 1")
        end
        it "creates new workspace" do
          expect do
            post :create, attributes_for(:workspace)
          end.to change(Workspace, :count).by(1)
        end
      end
      context "with incorrect data" do
        it "returns unprocessable_entity status" do
          post :create, attributes_for(:workspace, name: "")
          expect(response).to have_http_status(:unprocessable_entity)
        end
        it "returns json errors array" do
          post :create, attributes_for(:workspace, name: "")
          errors = JSON.parse(response.body)
          expect(errors.size).to be > 0
        end
        it "does not create new workspace" do
          expect do
            post :create, attributes_for(:workspace, name: "")
          end.to change(Workspace, :count).by(0)
        end
      end
    end
  end

  context "requests with incorrect authentication token" do
    before { request.env['HTTP_AUTHORIZATION'] = "Token token=\"blabla\"" }
    describe "GET index" do
      it "returns unauthorized status" do
        get :index
        expect(response).to have_http_status(:unauthorized)
      end
    end
    describe "GET show" do
      it "returns unauthorized status" do
        get :show, id: 14
        expect(response).to have_http_status(:unauthorized)
      end
    end
    describe "POST create" do
      it "returns unauthorized status" do
        post :create, attributes_for(:workspace)
        expect(response).to have_http_status(:unauthorized)
      end
    end
    describe "PUT update" do
      it "returns unauthorized status" do
        put :update, attributes_for(:workspace, id: 13)
        expect(response).to have_http_status(:unauthorized)
      end
    end
    describe "DELETE destroy" do
      it "returns unauthorized status" do
        delete :destroy, id: 13
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end
end