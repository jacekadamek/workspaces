module Api
  class ApiBaseController < ActionController::Base
    before_filter :authenticate_from_token!

    private

    def authenticate_from_token!
      authenticate_or_request_with_http_token do |token, options|
        user = User.find_by(authentication_token: token)
        self.api_user = user
        user
      end
    end

    attr_accessor :api_user
  end
end